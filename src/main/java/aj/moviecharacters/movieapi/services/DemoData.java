package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.models.Movie;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import aj.moviecharacters.movieapi.repositories.CharacterRepository;
import aj.moviecharacters.movieapi.repositories.FranchiseRepository;
import aj.moviecharacters.movieapi.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


@Service
public class DemoData implements CommandLineRunner {

    Logger logger = LoggerFactory.getLogger(DemoData.class);

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Override
    public void run(String...args) throws Exception {

        // Creating Franchise:
        Franchise avengers  = new Franchise();
        avengers.setName("The Avengers");
        avengers.setDescription("A franchise where every movie plays as an homage to the matrix");
        franchiseRepository.save(avengers);
        logger.info("Saved Avengers Franchise");

        Franchise dollarsTrilogy  = new Franchise();
        dollarsTrilogy.setName("Dollars Trilogy");
        dollarsTrilogy.setDescription("It's complicated.");
        franchiseRepository.save(dollarsTrilogy);
        logger.info("Saved Dollars Trilogy Franchise");


        // Creating Movies:
        // Dollars Trilogy Franchise:

        /* question: could you make a default genre/director in a franchise so that
           if you don't set genre/director it just set the default one automatically

           instead of writing logger string manually, could maybe build string of attributes
            and print that? for saved movie?  @justEpsilon
         */
        Movie dollars = new Movie();
        String title = dollars.setTitle("A Fistful of Dollars"); //initializing title. Changed setter to return the title.
        dollars.setGenre("Spaghetti Western");
        dollars.setYear(1964);
        dollars.setDirector("Sergio Leone");
        dollars.setPosterUrl("https://dollarposter.com");//not real
        dollars.setTrailer("https://fulldollars.dk");//not real

        dollars.setFranchise(dollarsTrilogy);
        movieRepository.save(dollars);
        logger.info("Saved movie: " + title);
        logger.info(dollars.printMovie());

        Movie dollars2 = new Movie();
        title = dollars2.setTitle("For a Few Dollars More");
        dollars2.setGenre("Action, Spaghetti Western");
        dollars2.setYear(1965);
        dollars2.setDirector("Enrico Morricone");//he is actually a composer.
        dollars2.setPosterUrl("https://posterposter.com");
        dollars2.setTrailer("https://whodis?.com");

        dollars2.setFranchise(dollarsTrilogy);
        movieRepository.save(dollars2);
        logger.info("Saved movie: " + title);

        Movie dollars3 = new Movie();
        title = dollars3.setTitle("The Good, The Bad and The Ugly");
        dollars3.setGenre("Action, Western");
        dollars3.setYear(1966);
        dollars3.setDirector("Sergio Leone");
        dollars3.setPosterUrl("https://p.com");
        dollars3.setTrailer("http://aaaa.rrrrr");

        dollars3.setFranchise(dollarsTrilogy);
        movieRepository.save(dollars3);
        logger.info("Saved movie: " + title);


        // The Avengers Franchise:
        Movie guardians = new Movie();
        guardians.setTitle("Guardians of the Galaxy");
        guardians.setGenre("Superhero");
        guardians.setYear(2014);
        guardians.setDirector("James Gunn");
        guardians.setPosterUrl("https://upload.wikimedia.org/wikipedia/en/3/33/Guardians_of_the_Galaxy_%28film%29_poster.jpg");
        guardians.setTrailer("https://www.youtube.com/watch?v=fcHKR_l5F1k");

        // connecting to Franchise:
        guardians.setFranchise(avengers);
        movieRepository.save(guardians);
        logger.info("Saved movie: Guardians of the Galaxy");

        Movie guardians2 = new Movie();
        guardians2.setTitle("Guardians of the Galaxy vol.2");
        guardians2.setGenre("Superhero");
        guardians2.addGenre("Action");
        guardians2.setYear(2017);
        guardians2.setDirector("James Gunn");
        guardians2.setPosterUrl("https://upload.wikimedia.org/wikipedia/en/3/33/Guardians_of_the_Galaxy_%28film%29_poster.jpg");
        guardians2.setTrailer("https://www.youtube.com/");

        guardians2.setFranchise(avengers);
        movieRepository.save(guardians2);
        logger.info("Saved movie: Guardians of the Galaxy Vol. 2");
        logger.info(guardians2.printMovie());

        Movie ironMan = new Movie();
        ironMan.setTitle("Iron Man");
        ironMan.setGenre("Superhero");
        ironMan.addGenre("Sci-fi");
        ironMan.setYear(2008);
        ironMan.setDirector("Jon Favreau");
        ironMan.setPosterUrl("https://th.bing.com/th/id/OIP.iZ4hQlj3io_bfl7oA1_bUQHaK-?pid=ImgDet&rs=1");
        ironMan.setTrailer("https://www.youtube.com/watch?v=8ugaeA-nMTc");

        // connecting to Franchise:
        ironMan.setFranchise(avengers);
        logger.info(ironMan.printMovie());// let me see
        movieRepository.save(ironMan);
        logger.info("Saved movie: Iron Man");
        logger.info(ironMan.printMovie());

        // Characters:

        // Dollars:
        /* eggSelent trilogy, you outdid yourself Andy!
            cuz names are different in each movie (Joe, Manco, Blondie) but alias is the same.
            could we somehow change the name in each movie, but have it to be same character,
            another Model, or just save uit as 3 characters?
         */
        
        MovieCharacter clintEastwood = new MovieCharacter();
        clintEastwood.setName("Joe");
        clintEastwood.setAlias("Man with No Name");
        clintEastwood.setGender("male");
        clintEastwood.setPictureUrl("https://cosmicbook.news/sites/default/files/whatever.jpg");// not real

        // connecting to a franchise:
        clintEastwood.setFranchise(dollarsTrilogy);
        // connecting to movies:
        clintEastwood.setMultMovies(dollars, dollars2, dollars3);

        characterRepository.save(clintEastwood);
        logger.info("Character saved: Clint Eastwood");

        // The Avengers:
        MovieCharacter chrisPratt = new MovieCharacter();
        chrisPratt.setName("Peter Quill ");
        chrisPratt.setAlias("Star Lord");
        chrisPratt.setGender("male");
        chrisPratt.setPictureUrl("https://cosmicbook.news/sites/default/files/chris-pratt-star-lord-guardians-galaxy-2-message.jpg");

        // New Character

        MovieCharacter oveSprogo = new MovieCharacter();
        oveSprogo.setName("egon olsen");
        oveSprogo.setAlias("egon");
        oveSprogo.setGender("male");
        oveSprogo.setPictureUrl("ff");
        characterRepository.save(oveSprogo);

        // connecting a franchise:
        chrisPratt.setFranchise(avengers);
        // connecting movies:
        chrisPratt.setMultMovies(guardians, guardians2);

        characterRepository.save(chrisPratt);
        logger.info("Character saved: Chris Pratt");

    }
}