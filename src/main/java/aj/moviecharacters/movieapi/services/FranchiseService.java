package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.dto.FranchiseDto;
import aj.moviecharacters.movieapi.dto.UpdateFranchiseDto;

public interface FranchiseService {
    FranchiseDto addFranchise(FranchiseDto franchiseDto);

    FranchiseDto updateFranchise(UpdateFranchiseDto updateFranchisesDto);

    FranchiseDto patchFranchise(FranchiseDto franchiseDto);


}
