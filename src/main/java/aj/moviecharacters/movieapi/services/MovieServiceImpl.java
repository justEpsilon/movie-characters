package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.dto.CharacterDto;
import aj.moviecharacters.movieapi.dto.FranchiseDto;
import aj.moviecharacters.movieapi.dto.MovieDto;
import aj.moviecharacters.movieapi.dto.UpdateMovieDto;
import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.models.Movie;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import aj.moviecharacters.movieapi.repositories.CharacterRepository;
import aj.moviecharacters.movieapi.repositories.FranchiseRepository;
import aj.moviecharacters.movieapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {

    @Resource
    MovieRepository movieRepository;

    @Resource
    FranchiseRepository franchiseRepository;

    @Resource
    CharacterRepository characterRepository;

    public void addAMovie(Movie movie) {
        if (movie != null) {
            movieRepository.save(movie);
        }
    }

    @Override
    public MovieDto addMovie(MovieDto movieDto) {

        Movie movieToSave = new Movie();
        mapDtoToEntity(movieDto, movieToSave);
        System.out.println(movieToSave.characterGetter());
        Movie movie = movieRepository.save(movieToSave);
        return mapEntityToDto(movie);
    }

    @Override
    public UpdateMovieDto updateMovie(UpdateMovieDto updateMovieDto) {

        Set<MovieCharacter> movieCharacterSet = new HashSet<>();
        for (long id : updateMovieDto.getCharacterIds()) {
            MovieCharacter movieCharacter = characterRepository.getById(id);
            movieCharacterSet.add(movieCharacter);
        }
        Movie movieToUpdate = movieRepository.getById(updateMovieDto.getIdToUpdate());
        movieToUpdate.setCharacters(movieCharacterSet);
        movieToUpdate.setTitle("uodated movie");
        movieRepository.save(movieToUpdate);

        //TODO make it return a responseDTO.
        return null;
    }

    @Override
    public MovieDto patchMovie(MovieDto movieDto) {

        // TODO: Ability to patch movies and franchises...
        Movie movieToPatch = movieRepository.getById(movieDto.getId());

        movieToPatch.setMovieId(movieDto.getId());
        if (movieDto.getTitle() != null) {
            movieToPatch.setTitle(movieDto.getTitle());
        }
        if (movieDto.getTrailer() != null) {
            movieToPatch.setTrailer(movieDto.getTrailer());
        }
        if (movieDto.getPosterUrl() != null) {
            movieToPatch.setPosterUrl(movieDto.getPosterUrl());
        }
        if (movieDto.getYear() == 0) {
            movieToPatch.setYear(movieDto.getYear());
        }
        if (movieDto.getGenre() != null) {
            movieToPatch.setGenre(movieDto.getGenre());
        }
        return null;
    }

    private void mapDtoToEntity(MovieDto movieDto, Movie movieToSave) {

        // Get existing franchise and add it to the movie.
        // Only adds franchises if they exist...
        // When we work with dto our connection also have to be as dtos, instead of models. 1 extra step:

        if (movieDto.getFranchiseDto() != null) {

            if(movieDto.getFranchiseDto().getId()==0){
                Franchise franchise = new Franchise();
                franchise.setName(movieDto.getFranchiseDto().getName());
                franchise.setDescription(movieDto.getFranchiseDto().getDescription());
                // Save transient before flushing. We can't refer to a FK not in the database yet. So we have to save it first.
                movieToSave.setFranchise(franchiseRepository.save(franchise));
            }else {
                Franchise franchise = franchiseRepository.getById(movieDto.getFranchiseDto().getId());
                movieToSave.setFranchise(franchise);
            }
        }

        movieToSave.setTitle(movieDto.getTitle());
        movieToSave.setTrailer(movieDto.getTrailer());
        movieToSave.setGenre(movieDto.getGenre());
        movieToSave.setDirector(movieDto.getDirector());
        movieToSave.setPosterUrl(movieDto.getPosterUrl());
        movieToSave.setYear(movieDto.getYear());

        //New character set.
        addCharacterFromDto(movieDto, movieToSave);

    }

    // Handles movieCharacters relation.
    private <T> void addCharacterFromDto(MovieDto movieDto, Movie movieToSave) {
        Set<MovieCharacter> actualCharacters = new HashSet();
        //Get existing characters and adds them to the movie.
        for (CharacterDto charElement : movieDto.getCharacters()) { // get characters from list from dto.

            MovieCharacter movieCharacter = characterRepository.getById(charElement.getId());
            Set<Movie> actualMovies = new HashSet<>();
            // Adding all the movies the character is already in to our new set.
            for (Movie movie : movieCharacter.getMovies()) {
                actualMovies.add(movie);
            }

            if (movieDto.getFranchiseDto() != null) {
                movieCharacter.setFranchise(movieToSave.getFranchise());
            }
            // Adding the movie we are adding to the characters set.
            actualMovies.add(movieToSave);
            movieCharacter.setMovies(actualMovies);
            actualCharacters.add(movieCharacter);

        }

        movieToSave.setCharacters(actualCharacters);
    }

    public MovieDto mapEntityToDto(Movie movie) {
        MovieDto responseDto = new MovieDto();
        responseDto.setDirector(movie.getDirector());
        responseDto.setGenre(movie.getGenre());
        FranchiseDto franchiseDto = new FranchiseDto();
        franchiseDto.setName(movie.getFranchise().getName());
        franchiseDto.setId(movie.getFranchise().getId());
        responseDto.setFranchiseDto(franchiseDto);
        //Set Characters from Integer Array.
        Set<CharacterDto> actualChars= new HashSet<>();
        for (MovieCharacter movieCharacter:movie.getCharacters()) {
            CharacterDto characterDto = new CharacterDto();
            characterDto.setId(movieCharacter.getId());
            actualChars.add(characterDto);
        }
        responseDto.setCharacters(actualChars);
        responseDto.setTrailer(movie.getTrailer());
        responseDto.setPosterUrl(movie.getPosterUrl());
        responseDto.setTitle(movie.getTitle());
        return responseDto;
    }
}
