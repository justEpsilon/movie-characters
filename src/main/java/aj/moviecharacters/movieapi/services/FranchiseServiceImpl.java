package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.dto.CharacterDto;
import aj.moviecharacters.movieapi.dto.FranchiseDto;
import aj.moviecharacters.movieapi.dto.UpdateFranchiseDto;
import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.models.Movie;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import aj.moviecharacters.movieapi.repositories.CharacterRepository;
import aj.moviecharacters.movieapi.repositories.FranchiseRepository;
import aj.moviecharacters.movieapi.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@Service
public class FranchiseServiceImpl implements FranchiseService {

    Logger logger = LoggerFactory.getLogger(FranchiseServiceImpl.class);

    @Resource
    MovieRepository movieRepository;

    @Resource
    FranchiseRepository franchiseRepository;
    @Resource
    CharacterRepository characterRepository;

    @Override
    public FranchiseDto addFranchise(FranchiseDto franchiseDto) {

        Franchise franchise = new Franchise();
        mapDtoToEntity(franchiseDto, franchise); // dto is the we getting and franchise is the one we mapping to.
        Franchise returnFranchise = franchiseRepository.save(franchise);
        return mapEntityToDto(returnFranchise);
    }


    @Override

    public FranchiseDto updateFranchise(UpdateFranchiseDto updateFranchisesDto) {

        Franchise franchiseToUpdate = franchiseRepository.getById(updateFranchisesDto.getId());
        Set<Movie> movies = new HashSet<>();
        for (Integer movieIdInt : updateFranchisesDto.getMovieIds()) {
            long movieId = movieIdInt.longValue();
            Movie movie = movieRepository.getById(movieId);
            movies.add(movie);
        }

        franchiseToUpdate.setMovies(movies);
        Franchise returnFranchise = franchiseRepository.save(franchiseToUpdate);
        logger.info("updated franchise successfully.");
        return mapEntityToDto(returnFranchise);

    }

    @Override
    public FranchiseDto patchFranchise(FranchiseDto franchiseDto) {
        // TODO: Ability to patch movies and characters might not be working 100%.

        Franchise franchise = franchiseRepository.getById(franchiseDto.getId());
        //Map (patch) to franchise.
        mapDtoToEntityPatch(franchiseDto, franchise);

        return mapEntityToDto(franchiseRepository.save(franchise));
    }

    private void mapDtoToEntityPatch(FranchiseDto franchiseDto, Franchise franchise) {
        if (franchiseDto.getMovies() != null) franchiseDto.setMovies(franchiseDto.getMovies());

        if (franchiseDto.getCharacters() != null) {
            Set<MovieCharacter> movieCharacters = new HashSet<>();
            for(MovieCharacter movieCharacter : franchiseDto.getCharacters()){
                long id = characterRepository.getById(movieCharacter.getId()).getId();
                if(id==0){ //If we encountered a new character
                    movieCharacters.add(movieCharacter);
                }else{ //If we encountered an old, add it to the list.
                    MovieCharacter movieCharacterToUpdate = characterRepository.getById(movieCharacter.getId());
                    movieCharacters.add(movieCharacterToUpdate);
                }
            }
            franchiseDto.setCharacters(movieCharacters);
        }
        if (franchiseDto.getName() != null) franchise.setName(franchiseDto.getName());
        if (franchiseDto.getDescription() != null) franchise.setDescription(franchiseDto.getName());

    }

    private void mapDtoToEntity(FranchiseDto franchiseDto, Franchise franchise) {
        Set<Movie> actualMovies = new HashSet<>();
        for (Movie movie : franchiseDto.getMovies()) {

            // id = 0 means movie doesn't exist
            long id = movie.getId();
            /* if movie doesn't exist - save the movie thus generating Id for it
                & add connection to franchise
                if movie exists add connection to franchise
            */
            if (id == 0) {
                Movie newMovie = movieRepository.save(movie);
                actualMovies.add(newMovie);
            } else {
                Movie existingMovie = movieRepository.getById(id); // if id = 0 it gets null
                actualMovies.add(existingMovie);
            }
        }

        franchiseDto.setMovies(actualMovies);
        franchise.setMovies(franchiseDto.getMovies());
        franchise.setName(franchiseDto.getName());
        franchise.setDescription(franchiseDto.getDescription());
        franchise.setId(franchiseDto.getId());

        Set<MovieCharacter> actualCharacters = new HashSet<>();

        for (MovieCharacter movieCharacter : franchiseDto.getCharacters()) {
            long id = movieCharacter.getId();
            /* if movie doesn't exist - save the movie thus generating Id for it
                & add connection to franchise
                if movie exists add connection to franchise
            */
            if (id == 0) {
                MovieCharacter newCharacter = characterRepository.save(movieCharacter);
                actualCharacters.add(newCharacter);
            } else {
                MovieCharacter existingCharacter = characterRepository.getById(id); // if id = 0 it gets null
                actualCharacters.add(existingCharacter);
            }
            franchise.setCharacters(actualCharacters);
        }

    }

    private FranchiseDto mapEntityToDto(Franchise franchise) {
        FranchiseDto responseDto = new FranchiseDto();
        responseDto.setMovies(franchise.getMovies());
        responseDto.setName(franchise.getName());
        responseDto.setDescription(franchise.getDescription());
        responseDto.setCharacters(franchise.getCharacters());
        responseDto.setId(franchise.getId());
        return responseDto;
    }


}
