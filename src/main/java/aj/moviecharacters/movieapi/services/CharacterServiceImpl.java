package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.dto.CharacterDto;
import aj.moviecharacters.movieapi.dto.UpdateCharacterDto;
import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.models.Movie;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import aj.moviecharacters.movieapi.repositories.CharacterRepository;
import aj.moviecharacters.movieapi.repositories.FranchiseRepository;
import aj.moviecharacters.movieapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

@Service
public class CharacterServiceImpl implements CharacterService {

    @Resource
    CharacterRepository characterRepository;

    @Resource
    MovieRepository movieRepository;

    @Resource
    FranchiseRepository franchiseRepository;

    public CharacterDto addCharacter(CharacterDto characterDto) {
        MovieCharacter character = new MovieCharacter();
        mapDtoToEntity(characterDto, character);
        MovieCharacter returnCharacter = characterRepository.save(character);
        return mapEntityToDto(returnCharacter);
    }

    @Override
    public UpdateCharacterDto updateCharacter(UpdateCharacterDto updateCharacterDto) {

        MovieCharacter characterToUpdate = characterRepository.getById(updateCharacterDto.getId());
        Set<Movie> movies = new HashSet<>();
        for (Integer movieIdInt : updateCharacterDto.getMovieIds()) {
            long movieId = movieIdInt.longValue();
            Movie movie = movieRepository.getById(movieId);
            movies.add(movie);
        }
        characterToUpdate.setMovies(movies);
        characterRepository.save(characterToUpdate);
        return null;
    }

    @Override
    public CharacterDto patchCharacter(CharacterDto characterDto) {
        // TODO: Ability to patch movies and franchises...
        MovieCharacter characterToPatch = characterRepository.getById(characterDto.getId());

        characterToPatch.setId(characterDto.getId());
        if (characterToPatch.getName() != null) {
            characterToPatch.setName(characterDto.getName());
        }
        if (characterDto.getAlias() != null) {
            characterToPatch.setAlias(characterDto.getAlias());
        }
        if (characterDto.getPictureUrl() != null) {
            characterToPatch.setPictureUrl(characterDto.getPictureUrl());
        }
        if (characterDto.getGender() !=null) {
            characterToPatch.setGender(characterDto.getGender());
        }

        return null;

    }

    // this might not work, if moviues already exist in movies, it will override it. Modify for put.
    private void mapDtoToEntity(CharacterDto characterDto, MovieCharacter character) {
        Set<Movie> actualMovies = new HashSet<>();
        for (Movie movie : characterDto.getMovies()) {

            // id = 0 means movie doesn't exist
            long id = movie.getId();
            /* if movie doesn't exist - save the movie thus generating Id for it
                & add connection to franchise
                if movie exists add connection to franchise
            */
            if (id == 0) {
                Movie newMovie = movieRepository.save(movie);
                actualMovies.add(newMovie);
            } else {
                Movie existingMovie = movieRepository.getById(id); // if id = 0 it gets null
                actualMovies.add(existingMovie);
            }
        }

        character.setId(characterDto.getId());
        character.setName(characterDto.getName());
        character.setAlias(characterDto.getAlias());
        character.setGender(characterDto.getGender());
        character.setPictureUrl(characterDto.getPictureUrl());

        character.setMovies(actualMovies);
        characterDto.setMovies(actualMovies);

        /**
         * one can add only one franchise, therefore we don't need to loop over the franchise ids.
         *
         * fix if the someone put multiple franchise.
         **/

        Franchise actualFranchise = characterDto.getFranchise();
        long id = 0;
        if (actualFranchise != null) {
            id = characterDto.getFranchise().getId();
        }

        if (id == 0) {
            if (actualFranchise != null) {
                actualFranchise = franchiseRepository.save(actualFranchise);
            }
        } else {
            actualFranchise = franchiseRepository.getById(id);
        }

        character.setFranchise(actualFranchise);
        characterDto.setFranchise(actualFranchise);


    }

    private CharacterDto mapEntityToDto(MovieCharacter character) {
        CharacterDto responseDto = new CharacterDto();
        responseDto.setId(character.getId());
        responseDto.setName(character.getName());
        responseDto.setAlias(character.getAlias());
        responseDto.setGender(character.getGender());
        responseDto.setPictureUrl(character.getPictureUrl());
        responseDto.setMovies(character.getMovies());

        if (character.getFranchise() != null) {
            responseDto.setFranchise(character.getFranchise());
        }


        return responseDto;
    }

}
