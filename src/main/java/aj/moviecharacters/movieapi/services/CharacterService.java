package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.dto.CharacterDto;
import aj.moviecharacters.movieapi.dto.UpdateCharacterDto;

public interface CharacterService {

    CharacterDto addCharacter(CharacterDto characterDto);
    UpdateCharacterDto updateCharacter(UpdateCharacterDto updateCharacterDto);
    CharacterDto patchCharacter(CharacterDto characterDto);

}
