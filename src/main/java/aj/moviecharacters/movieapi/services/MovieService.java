package aj.moviecharacters.movieapi.services;

import aj.moviecharacters.movieapi.dto.MovieDto;
import aj.moviecharacters.movieapi.dto.UpdateMovieDto;

public interface MovieService {
    MovieDto addMovie(MovieDto movieDto);
    UpdateMovieDto updateMovie(UpdateMovieDto updateMovieDto);
    MovieDto patchMovie(MovieDto movieDto);
}
