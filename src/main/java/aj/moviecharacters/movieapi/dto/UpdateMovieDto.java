package aj.moviecharacters.movieapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateMovieDto {
    private long[] characterIds;
    private long idToUpdate;
}
