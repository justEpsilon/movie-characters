package aj.moviecharacters.movieapi.dto;

import aj.moviecharacters.movieapi.models.Movie;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.*;
import java.util.stream.Collectors;

public class FranchiseDto {
    private long id;
    private String name;
    private Set<Movie> movies;
    private Set<MovieCharacter> characters;
    private String description;

    @JsonGetter
    public List<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    // Franchise to Character:

    /* changed to public, so it can be accessed from controllers or services.
        name has to be the same as a name of the attribute in data structure (e.g. json file).
        e.g. movieSet - no good.

        Not sure about this bit, maybe refactor back to Character(take care not to import util)
        before moving on (improper naming sucks!!!)
        @justEpsilon
     */

    @JsonGetter("characters")
    public List<String> characters() {
        if (characters != null) {
            return characters.stream()
                    .map( character -> {
                        return "api/v1/characters/" + character.getId();
                    }).collect(Collectors.toList());
        }
        return null;

    }

    // Getters:
    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public Set<Movie> getMovies() {
        return movies;
    }
    public Set<MovieCharacter> getCharacters() {
        return characters;
    }

    public String getDescription() {
        return description;
    }



    // Setters:
    public void setId(long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
    public void setCharacters(Set<MovieCharacter> characters) {
        this.characters = characters;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    // Other:
    public void addMovieToSet(Movie movie){
        this.movies.add(movie);
    }
}
