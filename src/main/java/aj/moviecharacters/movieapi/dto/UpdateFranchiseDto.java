package aj.moviecharacters.movieapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateFranchiseDto {
    private Integer[] movieIds;
    private long id;
}
