package aj.moviecharacters.movieapi.dto;

import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.models.Movie;
import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
/**
 *  Should work now: try {
 *     "name": "Jim",
 *     "alias": "Mij",
 *     "gender": "undef",
 *     "movies": [
 *         {
 *             "id": 1
 *         },
 *         {
 *             "id": 2
 *         }
 *     ]
 * }
 * Franchises could be added as well.
 */
public class CharacterDto {

	private long id;
	private String name; // full name
	private String alias;
	private String gender;
	private String pictureUrl;
	private Set<Movie> movies;
	private Franchise franchise;

	// Character to Movie:


	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}


    /* changed to public, so it can be accessed from controllers or services.
        name has to be the same as a name of the attribute in data structure (e.g. json file).
        e.g. movieSet - no good. @justEpsilon
     */

	@JsonGetter("movies")
	public List<String> movies() {
		return movies.stream()
				.map(movie -> {
					return "/api/v1/movies/" + movie.getId();
				}).collect(Collectors.toList());

	}

	@JsonGetter("franchise")
	public String franchise() {
		if (franchise != null) {
			return "api/v1/franchise/" + franchise.getId();
		}
		return null;
	}



	/* Will have to change this,
        now I am using it in demoData for multiple movies to a character.
        maybe a good idea, actually, to @override it so both Set and Array could be input
     */
	public void setMultMovies(Movie... movies) {
		this.movies = Set.of(movies);
	}
}