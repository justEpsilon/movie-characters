package aj.moviecharacters.movieapi.dto;

import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MovieDto {


    private long id;
    private String title;
    private FranchiseDto franchiseDto;
    private String genre;
    private int year;
    private String director;
    private String posterUrl;
    private String trailer; //YouTube link most likely

    Set<CharacterDto> characters = new HashSet<>();

    @JsonProperty("franchiseDto")
    public String franchiseGetter() {
        if (franchiseDto != null) {
            return "/api/v1/franchises/" + franchiseDto.getId();
        } else {
            return null;
        }
    }

    // this will be a git conflict, getCharachterId vs. getID should be getID!!!!!!!
    @JsonProperty("characters")
    public List<String> characters() {
        List<String> returnString = new LinkedList<>();
        if (characters != null) {
            for (CharacterDto character: characters) {
                returnString.add("/api/v1/characters/" + character.getId());
            }
            return returnString;
        }
        return null;
    }

    // Getters:

    // check if this(fetMovieId) is why the postmen doesn't work with ID as intended:


    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public int getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getTrailer() {
        return trailer;
    }

    public FranchiseDto getFranchiseDto() {
        return franchiseDto;
    }

    public long getId() {
        return id;
    }



    // Setters:

    // maybe if we did not override this method, the postmen could use Id: instead of movieId.

    public String setTitle(String title) {
        this.title = title;
        return title;// this is so it is easy to print in terminal
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public void setFranchiseDto(FranchiseDto franchiseDto) {
        this.franchiseDto = franchiseDto;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCharacters(Set<CharacterDto> characters) {
        this.characters = characters;
    }

    public Set<CharacterDto> getCharacters() {
        return characters;
    }

    // Other: (e.g. some helper functions for debugging | just trying things out )


    public void addGenre(String genre) {
        this.genre += ", " + genre;

    }


}
