package aj.moviecharacters.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "genre")
    private String genre;
    @Column(name = "year")
    private int year;
    @Column(name = "director")
    private String director;
    @Column(name = "url")
    private String posterUrl;
    @Column(name = "trailer")
    private String trailer; //YouTube link most likely

    public Franchise getFranchise() {
        return franchise;
    }

    // Movie To Franchise:
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchiseGetter() {
        if (franchise != null) {
            return "/api/v1/franchises/" + franchise.getId();
        } else {
            return null;
        }
    }


    public void setCharacters(Set<MovieCharacter> characters) {
        this.characters = characters;
    }

    // Movie to Character:
    @ManyToMany(mappedBy = "movies")
    public Set<MovieCharacter> characters;

    @JsonGetter("characters")
    public List<String> characterGetter() {
        if (characters != null) {
            return characters.stream()
                    .map(character -> {
                        return "/api/v1/characters/" + character.getId();
                    }).collect(Collectors.toList());

        }
        return null;
    }

    // Getters:

    // check if this(fetMovieId) is why the postmen doesn't work with ID as intended:
    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public int getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public String getTrailer() {
        return trailer;
    }


    // Setters:

    // maybe if we did not override this method, the postmen could use Id: instead of movieId.
    public void setMovieId(long movieId) {
        this.id = movieId;
    }

    public String setTitle(String title) {
        this.title = title;
        return title;// this is so it is easy to print in terminal
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }


    // Other: (e.g. some helper functions for debugging | just trying things out )

    public String printMovie() {
        String movie = (this.id + ". " + this.title + " (" + this.year + ")  " + "Genres: " + this.genre + ";  Director: " + this.director);
        //System.out.println(movie);
        return movie;
    }

    public void addGenre(String genre) {
        this.genre += ", " + genre;

    }

    public Set<MovieCharacter> getCharacters() {
        return characters;
    }
}
