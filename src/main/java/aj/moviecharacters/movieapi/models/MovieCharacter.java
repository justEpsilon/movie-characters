package aj.moviecharacters.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name; // full name
    @Column(name = "alias")
    private String alias;
    @Column(name = "gender")
    private String gender;
    @Column(name = "url")
    private String pictureUrl;



    // Character to Movie:

    @ManyToMany()
    @Column(name="character_id")
    @JoinTable(
            name = "movie_movie_character",
            joinColumns = {@JoinColumn(name = "movie_character_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")}
    )

    /* changed to public, so it can be accessed from controllers or services.
        name has to be the same as a name of the attribute in data structure (e.g. json file).
        e.g. movieSet - no good. @justEpsilon
     */
    private Set<Movie> movies;

    @JsonGetter("movies")
    public List<String> movies() {
        return movies.stream()
                .map(movie -> {
                    return "/api/v1/movies/" + movie.getId();
                }).collect(Collectors.toList());

    }

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchise() {
        if (franchise != null) {
            return "api/v1/franchise/" + franchise.getId();
        }
        return null;
    }


    // Getters:

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getGender() {
        return gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public Franchise getFranchise() {
        return this.franchise;
    }

    // Setters:

    public void setId(long characterId) {
        this.id = characterId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public void setMovies(Set<Movie> movies){this.movies = movies;}
    // Setters for inter-tabular connections:

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;

    }


    /* Will have to change this,
        now I am using it in demoData for multiple movies to a character.
        maybe a good idea, actually, to @override it so both Set and Array could be input
     */
        public void setMultMovies (Movie...movies){
            this.movies = Set.of(movies);
    }

}
