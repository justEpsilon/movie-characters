package aj.moviecharacters.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name="description")
    private String description;

    // Franchise to Movie:
    @OneToMany
    @JoinColumn(name = "franchise_id")
        /* changed to public, so it can be accessed from controllers or services.
        name has to be the same as a name of the attribute in data structure (e.g. json file).
        e.g. movieSet - no good. @justEpsilon
     */
    private Set<Movie> movies;

    @JsonGetter
    public List<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }


    // Franchise to Character:
    @OneToMany
    @JoinColumn(name = "character_id")
    /* changed to public, so it can be accessed from controllers or services.
        name has to be the same as a name of the attribute in data structure (e.g. json file).
        e.g. movieSet - no good.

        Not sure about this bit, maybe refactor back to Character(take care not to import util)
        before moving on (improper naming sucks!!!)
        @justEpsilon
     */
    public Set<MovieCharacter> characters;

    @JsonGetter("character")
    public List<String> characters() {
        if (characters != null) {
            return characters.stream()
                    .map(character -> {
                        return "api/v1/characters/" + character.getId();
                    }).collect(Collectors.toList());
        }
        return null;

    }

    // Getters:
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public Set<MovieCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<MovieCharacter> characters) {
        this.characters = characters;
    }
    public String getDescription() {
        return description;
    }

    // Setters:
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setId(long id) {
        this.id = id;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    // Other:
    // do we need it still? (it's a little late don't want to brake project before bed.)
    public void addMovieToSet(Movie movie) {
        this.movies.add(movie);
    }
}
