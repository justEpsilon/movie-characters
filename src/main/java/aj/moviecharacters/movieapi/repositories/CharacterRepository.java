package aj.moviecharacters.movieapi.repositories;

import aj.moviecharacters.movieapi.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<MovieCharacter,Long>{
//    Character getCharacterByName(String name);
//    Character findCharacterByAlias(String alias);
//    Boolean existsById(long Id);
//    Character findById(long Id);
}
