package aj.moviecharacters.movieapi.repositories;

import aj.moviecharacters.movieapi.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie,Long> {
//    Movie getMovieByTitleAndOrderByTitleDesc(String title);
//    Movie findMovieByTitleOrYearAfterOrderByTitleDesc(String title, int year);
//    Movie findMovieByGenreOrderByTitleDesc(String Genre);
//    Movie findMovieByDirectorOrderByDirectorDesc(String Director);
//    Boolean existsById(long Id);
}
