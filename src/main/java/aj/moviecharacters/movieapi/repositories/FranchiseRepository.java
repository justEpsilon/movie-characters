package aj.moviecharacters.movieapi.repositories;

import aj.moviecharacters.movieapi.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise,Long> {
//    Franchise findByNameOrderByNameDesc(String name);
//    Boolean existsByNameOrderByName(String name);
}
