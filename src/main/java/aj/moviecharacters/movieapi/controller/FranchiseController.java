package aj.moviecharacters.movieapi.controller;

import aj.moviecharacters.movieapi.dto.FranchiseDto;
import aj.moviecharacters.movieapi.dto.UpdateFranchiseDto;
import aj.moviecharacters.movieapi.models.Franchise;
import aj.moviecharacters.movieapi.repositories.MovieRepository;
import aj.moviecharacters.movieapi.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import aj.moviecharacters.movieapi.repositories.FranchiseRepository;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseService franchiseService;

    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchise() {
        List<Franchise> libraries = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(libraries, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        // We first check if the Library exists, this saves some computing time.
        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);
    }

    @PostMapping()
    public ResponseEntity<FranchiseDto> addFranchise(@RequestBody FranchiseDto franchiseDto) {
        FranchiseDto dtoToAdd = franchiseService.addFranchise(franchiseDto);
        return new ResponseEntity<>(dtoToAdd, HttpStatus.CREATED);
    }


    @PutMapping
    public ResponseEntity<FranchiseDto> updateFranchise(@PathVariable Long id, @RequestBody UpdateFranchiseDto updateFranchisesDto) {

        updateFranchisesDto.setId(id);

        FranchiseDto dtoToAdd = franchiseService.updateFranchise(updateFranchisesDto);

        return new ResponseEntity<>(dtoToAdd, HttpStatus.ACCEPTED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<FranchiseDto> patchFranchise(@PathVariable long id, @RequestBody FranchiseDto franchiseDto) {
        franchiseDto.setId(id);
        FranchiseDto responseDto = franchiseService.patchFranchise(franchiseDto);
        return new ResponseEntity<>(responseDto, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteFranchise(@PathVariable long id) {
        HttpStatus status = HttpStatus.OK;
        franchiseRepository.deleteById(id);
        return new ResponseEntity<>(true, status);
    }


}
