package aj.moviecharacters.movieapi.controller;

import aj.moviecharacters.movieapi.dto.CharacterDto;
import aj.moviecharacters.movieapi.dto.FranchiseDto;
import aj.moviecharacters.movieapi.models.MovieCharacter;
import aj.moviecharacters.movieapi.services.CharacterService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import aj.moviecharacters.movieapi.repositories.CharacterRepository;

import java.util.List;

@Tag(name = "Character", description = "the Character API")
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private CharacterService characterService;

    @GetMapping()
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        List<MovieCharacter> character = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(character, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getCharacter(@PathVariable Long id) {
        MovieCharacter returnCharacter = new MovieCharacter();
        HttpStatus status;
        // We first check if the Character exists, this saves some computing time.
        if (characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnCharacter, status);
    }

    @PostMapping
    public ResponseEntity<CharacterDto> addCharacter(@RequestBody CharacterDto characterDto) {
        HttpStatus status;
        CharacterDto responseDto = characterService.addCharacter(characterDto);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(responseDto, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter(@PathVariable Long id, @RequestBody MovieCharacter movieCharacter) {
        MovieCharacter returnCharacter = new MovieCharacter();
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        if (!id.equals(movieCharacter.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter, status);
        }
        returnCharacter = characterRepository.save(movieCharacter);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteCharacter(@PathVariable long id) {
        HttpStatus status = HttpStatus.OK;
        characterRepository.deleteById(id);
        return new ResponseEntity<>(true, status);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<CharacterDto> patchFranchise(@PathVariable long id, @RequestBody CharacterDto characterDto) {
        characterDto.setId(id);
        CharacterDto responseDto = characterService.patchCharacter(characterDto);
        return new ResponseEntity<>(responseDto, HttpStatus.ACCEPTED);
    }

}
