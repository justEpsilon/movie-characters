package aj.moviecharacters.movieapi.controller;

import aj.moviecharacters.movieapi.dto.FranchiseDto;
import aj.moviecharacters.movieapi.dto.MovieDto;
import aj.moviecharacters.movieapi.dto.UpdateMovieDto;
import aj.moviecharacters.movieapi.models.Movie;
import aj.moviecharacters.movieapi.repositories.MovieRepository;

import aj.moviecharacters.movieapi.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieService movieService;

    @GetMapping()

    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) {
        Movie returnMovie = new Movie();
        HttpStatus status;
        // We first check if the Movie exists, this saves some computing time.
        if (movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    @PostMapping
    public ResponseEntity<MovieDto> addMovie(@RequestBody MovieDto movieDto) {
        HttpStatus status = HttpStatus.CREATED;
        MovieDto returnDto = movieService.addMovie(movieDto);

        return new ResponseEntity<>(returnDto, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable long id, @RequestBody UpdateMovieDto updateMovieDto) {
        HttpStatus status;
        /*
         We want to check if the request body matches what we see in the path variable.
         This is to ensure some level of security, making sure someone
         hasn't done some malicious stuff to our body.
        */
        updateMovieDto.setIdToUpdate(id);

        movieService.updateMovie(updateMovieDto);

        status = HttpStatus.OK;
        return new ResponseEntity<>(status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteFranchise(@PathVariable long id) {
        HttpStatus status = HttpStatus.OK;
        movieRepository.deleteById(id);
        return new ResponseEntity<>(true, status);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<MovieDto> patchFranchise(@PathVariable long id, @RequestBody MovieDto movieDto) {
        movieDto.setId(id);
        MovieDto responseDto = movieService.patchMovie(movieDto);
        return new ResponseEntity<>(responseDto, HttpStatus.ACCEPTED);
    }


}
